package BTEP;

import BTEP.services.ManagerService;
import BTEP.services.ManagerServiceImpl;
import BTEP.tools.KeyboardUtility;

public class FestivalApp {


    public static void main(String[] args) {


        ManagerService managerService = new ManagerServiceImpl();

        managerService.start();

        KeyboardUtility.KEYBOARD.close();
        }

    }
