package BTEP.festival;

import BTEP.artists.Genre;
import BTEP.tools.MenuUtility;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;

public class Festival {

    private String festivalName;

    private FestivalDay[] duration;

    private Venue venue;

    private double dayPrice;

    private Genre genre;


    public Festival(String festivalName, Venue venue, double dayPrice, Genre genre, int dayAmount, LocalDate day, LocalTime startTime) {
        this.festivalName = festivalName;
        this.duration = new FestivalDay[dayAmount];
        this.venue = venue;
        this.dayPrice = dayPrice;
        this.genre = genre;

        int ticketsleft = this.venue.getCapacity();

        for (int i = 0; i < duration.length; i++) {
            this.duration[i] = new FestivalDay(day.plusDays(i), startTime, ticketsleft, (i+1));

        }


    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public FestivalDay[] getDuration() {
        return duration;
    }

    public void setDuration(FestivalDay[] duration) {
        this.duration = duration;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public double getDayPrice() {
        return dayPrice;
    }

    public void setDayPrice(double dayPrice) {
        this.dayPrice = dayPrice;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public int getNumberOfDays(){
        return duration.length;
    }

    public FestivalDay getDay(int dayNumber){
        return duration[dayNumber];
    }

    public void displayEvent() {
        System.out.printf("Details of your event: %n" +
                "\tfestival name : %s%n" +
                "\tevent duration: %d days%n",
                getFestivalName(),getNumberOfDays());

    }

    public void displayWholeLineup(){
        for (int i = 0; i< duration.length ; i++){
            duration[i].showLineup();
        }
    }


    @Override
    public String toString(){

        return (festivalName + "\n" + venue.getAdress() + "\n" + genre + "\n" + duration[0].getDay() +"\n" + MenuUtility.thinLine());

    }
}
