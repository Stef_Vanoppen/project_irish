package BTEP.festival;

public class Venue {
    public Venue(String adress, int capacity) {
        this.adress = adress;
        this.capacity = capacity;
    }

    private String adress;
    private int capacity;

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
