package BTEP.festival;


import BTEP.artists.Act;
import BTEP.tools.MenuUtility;

import java.time.LocalDate;
import java.time.LocalTime;

public class FestivalDay {


    private int dayNumber;

    private LocalDate day;

    private LocalTime startTime;

    private Act[] lineup = new Act[24];

    private int ticketsLeft;

    public FestivalDay(LocalDate day, LocalTime startTime, int ticketsLeft, int dayNumber) {
        this.day = day;
        this.startTime = startTime;
        this.ticketsLeft = ticketsLeft;
        this.dayNumber = dayNumber;
    }

    public LocalTime getStartTime() {
        return startTime;
    }


    public LocalDate getDay() {
        return day;
    }

    public void showLineup(){
        System.out.println(MenuUtility.thickLine());
        System.out.println("day " + dayNumber + " date: " + day + "\n" + "\t\t\t"+ startTime);
        System.out.println(MenuUtility.thickLine());
        for(int i = 0; i < lineup.length; i++){
            if(lineup[i] != null){
                if(lineup[i].isFirstHour()){
                    System.out.println(lineup[i].getTalent().getTalentName() + "\t" + startTime.plusHours(i) + " - "
                            + startTime.plusHours(i + lineup[i].getDuration()) + "\n" + MenuUtility.thinLine());
                }

            }
        }
    }

    public boolean[] showAvailableSpots(){
        System.out.println("available spots.");
        boolean[] isAvailable = new boolean[24];
        for(int i = 0; i < lineup.length; i++){
            if(lineup[i] == null){
                System.out.println((i+1) + "\t" + startTime.plusHours(i));
                isAvailable[i] =true;
            }else{isAvailable[i] = false;}


        }
        return isAvailable;

    }


    public void addAct(Act act, int slot){

        this.lineup[slot] =act;
        if(act.getDuration() > 1){
            Act nextHour = new Act(startTime.plusHours(slot+1),act.getDuration()-1, act.getTalent(),false);
            addAct(nextHour,(slot+1));
        }

    }


}
