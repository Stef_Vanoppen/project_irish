package BTEP.festival;

import BTEP.artists.Genre;
import BTEP.tools.KeyboardUtility;
import BTEP.tools.MenuUtility;

import java.time.LocalDate;
import java.time.LocalTime;


public class CreateFestival {

    public static Festival createFestival() {

        String name = KeyboardUtility.ask("Name of event: ");
        Venue venue = new Venue(KeyboardUtility.ask("Venue's address: "),
                KeyboardUtility.askForInt("Maximum patrons: "));


        for (int i = 1; i < 7; i++) {
            System.out.println(i + "\t" + Genre.valueOf(i));
        }

        Genre chooseGenre = Genre.valueOf(0);

        while(true) {
            int genreChoice = KeyboardUtility.askForInt("Genre: ");
            if(genreChoice > 6 || genreChoice < 1 ) {
                System.out.println("No such Genre.");
            }else{
                chooseGenre = Genre.valueOf(genreChoice);
                System.out.println(chooseGenre);
                break;
            }
        }

        double dayPrice = KeyboardUtility.askForDouble("Price/day: ");

        LocalDate day = KeyboardUtility.askForFullDate("Opening day: ");

        LocalTime startTime = LocalTime.of(KeyboardUtility.askForInt("Starting hour"), 0, 0);

        int numberOfDays = KeyboardUtility.askForInt("Event's duration in days: ");

        Festival festivalA = new Festival(name, venue,
                dayPrice,chooseGenre, numberOfDays, day, startTime);

        System.out.println(MenuUtility.thickLine());
        System.out.println(festivalA.toString());

        return festivalA;




    }
}
