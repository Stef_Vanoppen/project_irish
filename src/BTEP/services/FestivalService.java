package BTEP.services;


import BTEP.artists.Act;
import BTEP.festival.Festival;

public interface FestivalService {

    void registerNewAct();
    void registerAct(Act act);
    void setFestival(Festival festival);
    void buyTickets();
    void viewBalanceSheet();
    void displayLineUp();

}
