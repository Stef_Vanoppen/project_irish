package BTEP.services;

import BTEP.artists.Act;
import BTEP.artists.CreateBand;
import BTEP.artists.CreateSolo;
import BTEP.festival.Festival;
import BTEP.tools.KeyboardUtility;
import BTEP.tools.MenuUtility;

public class FestivalServiceImpl implements FestivalService {

    private Festival festival;

    private ActService actService;

    // private TicketService ticketService;


    @Override
    public void registerNewAct() {
        System.out.println(MenuUtility.thickLine());
        String[] festOptions = new String[]{"Solo act", "Band"};
        switch (KeyboardUtility.askForChoice("choose an option", festOptions)) {


            case 0:
                CreateSolo.createSolo(festival);
                break;
            case 1:
                CreateBand.createBand(festival);
                break;


        }

    }

    @Override
    public void registerAct(Act act) {

    }

    @Override
    public void setFestival(Festival festival) {
        this.festival = festival;
        System.out.println(MenuUtility.thickLine());
        System.out.println(festival.toString());

        String[] festOptions = new String[]{"register act", "show line up", "buy tickets"};
        switch (KeyboardUtility.askForChoice("choose an option", festOptions)) {


            case 0:
                registerNewAct();
                break;
            case 1:
                displayLineUp();
                break;
            case 2:
                buyTickets();
                break;

        }

    }

    @Override
    public void buyTickets() {
        System.out.println("coming soon.");

    }

    @Override
    public void viewBalanceSheet() {

    }

    @Override
    public void displayLineUp() {
        festival.displayWholeLineup();

    }
}
