package BTEP.services;

import BTEP.artists.Talent;

public interface TalentService {

    Talent createTalent();
}