package BTEP.services;

import BTEP.Menu;
import BTEP.artists.Genre;
import BTEP.festival.CreateFestival;
import BTEP.festival.Festival;
import BTEP.festival.Venue;
import BTEP.tools.KeyboardUtility;
import BTEP.tools.MenuUtility;

import java.time.LocalDate;
import java.time.LocalTime;

public class ManagerServiceImpl implements ManagerService {

    private FestivalService festivalService;

    private Festival[] festivals;


    @Override
    public void chooseFestival() {
        System.out.println(MenuUtility.thickLine());

        String[] festivalNames = new String[festivals.length];


        for (int i = 0; i < festivals.length; i++) {
            if (festivals[i] != null) {
                festivalNames[i] = festivals[i].toString();

            }

        }

        Festival chosenFestival = festivals[KeyboardUtility.askForChoice(festivalNames)];


        if(chosenFestival != null) {
            festivalService = new FestivalServiceImpl();
            festivalService.setFestival(chosenFestival);
        }else{
            System.out.println("no such festival.");
        }

    }
    @Override
    public void newFestival(){
        System.out.println(MenuUtility.thinLine());

        for(int i = 0; i < festivals.length; i++){
            if(festivals[i] == null){
                festivals[i] = CreateFestival.createFestival();
                break;
            }
        }

    }

    @Override
    public void serveFestival() {

    }

    @Override
    public void start() {

        System.out.println(MenuUtility.thickLine());
        System.out.println(MenuUtility.center("TEAM IRISH"));
        System.out.println(MenuUtility.center("'It just works!'"));
        System.out.println(MenuUtility.thickLine());
        festivals = new Festival[5];

        Venue testVen = new Venue("fakestreet 1", 420);
        festivals[0] = new Festival("Throatfest", testVen, 50, Genre.MONGOLIAN_THROAT_SINGING,
                3, LocalDate.of(2021, 5, 5), LocalTime.of(8, 0, 0));

        Venue otherVen = new Venue("fakestreet 2", 420);
        festivals[1] = new Festival("Bauxite importers convention", otherVen, 50, Genre.METAL,
                3, LocalDate.of(2021, 5, 5), LocalTime.of(8, 0, 0));

        boolean keepGoing = true;


        while (keepGoing) {

            String[] options = {"new festival", "choose festival", "exit"};


            switch (KeyboardUtility.askForChoice("choose an option", options)) {


                case 0:
                    newFestival();
                    break;
                case 1:
                    chooseFestival();
                    break;
                case 2:
                    System.out.println(MenuUtility.thickLine());
                    System.out.println(MenuUtility.center("♦ Goodbye ♦"));
                    System.out.println(MenuUtility.thickLine());
                    keepGoing = false;
                    break;

            }




        }
    }
}
