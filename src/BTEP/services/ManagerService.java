package BTEP.services;

public interface ManagerService {

   void newFestival();
   void chooseFestival();
   void serveFestival();
   void start();


}
