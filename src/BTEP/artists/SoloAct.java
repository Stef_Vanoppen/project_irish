package BTEP.artists;

import BTEP.person.Artist;

public class SoloAct extends  Talent{

    private Artist artist;

    public SoloAct(String talentName, Artist artist) {
        super(talentName);
        this.artist = artist;
    }
}
