package BTEP.artists;

import BTEP.festival.Festival;
import BTEP.festival.FestivalDay;
import BTEP.festival.Venue;
import BTEP.tools.KeyboardUtility;

import java.time.LocalDate;
import java.time.LocalTime;

public class CreateAct {
    public static void createAct(Talent talent, Festival testFest){



        System.out.println("This festival has " + testFest.getNumberOfDays() + " days.");

        FestivalDay day = KeyboardUtility.askForDay(testFest);

        day.showLineup();

        boolean[] available = day.showAvailableSpots();

        int timePick = 0;

        while(true){

            timePick = KeyboardUtility.askForInt("Pick a time (number in front):");
            if(timePick > 24 || timePick <1 || !available[(timePick - 1)]){
                System.out.println("You can't pick that time");
            }else{
                break;
            }


        }
        int amountOfHours = 1;
        boolean pog = true;
        while(pog){

            amountOfHours = KeyboardUtility.askForInt("how many hours?");
            if(amountOfHours > (24 - timePick) || !available[(timePick-1)+(amountOfHours-1)]){
                System.out.println("You don't have that much time");
            }else{
                pog= false;
                System.out.println("k");
            }


        }



        Act act = new Act(day.getStartTime().plusHours((timePick-1)),amountOfHours,talent);

        day.addAct(act,(timePick-1));

        testFest.displayWholeLineup();










    }
}
