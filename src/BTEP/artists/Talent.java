package BTEP.artists;

public abstract class Talent {

    private String talentName;

    public Talent(String talentName) {
        this.talentName = talentName;
    }

    public String getTalentName() {
        return talentName;
    }

    public void setTalentName(String talentName) {
        this.talentName = talentName;
    }
}

