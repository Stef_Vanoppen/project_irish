package BTEP.artists;

import java.util.HashMap;
import java.util.Map;

public enum Genre {
    ROCK(1),
    METAL(2),
    POP(3),
    ELECTRO(4),
    FOLK(5),
    MONGOLIAN_THROAT_SINGING(6);

    private  int value;
    private static Map map = new HashMap<>();

    private Genre(int value) {
        this.value = value;
    }

    static {
        for (Genre genre : Genre.values()) {
            map.put(genre.value, genre);
        }

    }

    public static Genre valueOf(int PageType) {
        return (Genre) map.get(PageType);
    }

    public int getValue() {
        return value;
    }
}
