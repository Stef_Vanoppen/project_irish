package BTEP.artists;

import BTEP.person.Artist;

public class Band extends Talent{

    private Artist[] members;

    public Band(String talentName, Artist[] members) {
        super(talentName);
        this.members = members;
    }
}
