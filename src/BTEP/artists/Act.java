package BTEP.artists;

import BTEP.festival.FestivalDay;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Act {

    private LocalTime startTime;

    private int durationHours;

    private Talent talent;

    private boolean isFirstHour;


    public Act(LocalTime startTime, int duration, Talent talent) {
        this.startTime = startTime;
        this.durationHours = duration;
        this.talent = talent;
        this.isFirstHour= true;


    }

    public Act(LocalTime startTime,int duration, Talent talent, boolean isFirstHour) {
        this.startTime = startTime;
        this.durationHours = duration;
        this.talent = talent;
        this.isFirstHour= isFirstHour;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public int getDuration() {
        return durationHours;
    }

    public void setDuration(int duration) {
        this.durationHours = duration;
    }

    public Talent getTalent() {
        return talent;
    }

    public boolean isFirstHour() {
        return isFirstHour;
    }

    public void setTalent(Talent talent) {
        this.talent = talent;
    }
}
