package BTEP.artists;

import BTEP.festival.Festival;
import BTEP.person.Artist;
import BTEP.person.CreateArtist;
import BTEP.tools.KeyboardUtility;

public class CreateSolo {
    public static void createSolo(Festival festival) {

        String talentName = KeyboardUtility.ask("Talent name: ");
        Artist artist = CreateArtist.createArtist();

        Talent solo = new SoloAct(talentName,artist);

        CreateAct.createAct(solo,festival);
    }
}
