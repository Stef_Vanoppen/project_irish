package BTEP.artists;

import BTEP.festival.Festival;
import BTEP.person.Artist;
import BTEP.person.CreateArtist;
import BTEP.tools.KeyboardUtility;

public class CreateBand {
    public static void createBand(Festival festival){

        String talentName = KeyboardUtility.ask("Talent name: ");

        Artist[] members = new Artist[KeyboardUtility.askForInt("How many members? ")];

        for(int i = 0; i < members.length; i++){
            System.out.println("Member " + (i+1));
            members[i] = CreateArtist.createArtist();
        }

        Talent band = new Band(talentName,members);

        CreateAct.createAct(band, festival);


    }
}
