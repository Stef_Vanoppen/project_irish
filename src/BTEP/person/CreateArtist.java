package BTEP.person;

import BTEP.tools.KeyboardUtility;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateArtist {
    public static Artist createArtist() {

        String name = KeyboardUtility.ask("Name: ");
        String surName = KeyboardUtility.ask("Surname: ");
        Gender gender = KeyboardUtility.askForGender("Gender: ");
        LocalDate dob = KeyboardUtility.askForFullDate("Date of birth: ");
        String  address = KeyboardUtility.ask("Address: ");
        String email = KeyboardUtility.askForEmail("E-mail: ");
        String instrument = KeyboardUtility.ask("Instrument: ");
        BigDecimal cashMoney = BigDecimal.valueOf(KeyboardUtility.askForDouble("Hourly Wage: "));

        Artist artBoi = new Artist(name,surName,gender,dob,address,email,cashMoney,instrument);

        return artBoi;


    }
}
