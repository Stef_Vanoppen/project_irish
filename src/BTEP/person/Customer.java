package BTEP.person;

import java.time.LocalDate;

public class Customer extends Person {

    private Long id;

    public Customer(String name, String surname, Gender gender, LocalDate dob, String address, String email, Long id) {
        super(name, surname, gender, dob, address, email);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
