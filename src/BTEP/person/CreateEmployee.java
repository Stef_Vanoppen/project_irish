package BTEP.person;

import BTEP.tools.KeyboardUtility;

import java.math.BigDecimal;
import java.time.LocalDate;

public class CreateEmployee {
    public static void main(String[] args) {

        String name = KeyboardUtility.ask("Name: ");
        String surName = KeyboardUtility.ask("Surname: ");
        Gender gender = KeyboardUtility.askForGender("Gender: ");
        LocalDate dob = KeyboardUtility.askForFullDate("Date of birth: ");
        String  address = KeyboardUtility.ask("Address: ");
        String email = KeyboardUtility.askForEmail("E-mail: ");
        BigDecimal cashMoney = BigDecimal.valueOf(KeyboardUtility.askForDouble("Hourly Wage: "));

        Employee wageSlave = new Employee(name,surName,gender,dob,address,email,cashMoney);


    }
}
