package BTEP.person;

import java.time.LocalDate;

public class Person {

    private String name;
    private String surname;
    private Gender gender;
    private LocalDate dob;
    private String address;
    private String Email;

    public Person(String name, String surname, Gender gender, LocalDate dob, String address, String email) {
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.dob = dob;
        this.address = address;
        Email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
