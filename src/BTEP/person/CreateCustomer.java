package BTEP.person;

import BTEP.tools.KeyboardUtility;

import java.time.LocalDate;
import java.util.Random;

public class CreateCustomer {
    public static void main(String[] args) {

        String name = KeyboardUtility.ask("Name: ");
        String surName = KeyboardUtility.ask("Surname: ");
        Gender gender = KeyboardUtility.askForGender("Gender: ");
        LocalDate dob = KeyboardUtility.askForFullDate("Date of birth: ");
        String  address = KeyboardUtility.ask("Address: ");
        String email = KeyboardUtility.askForEmail("E-mail: ");
        Random randId = new Random(999999999);
        Long id = randId.nextLong();

        Customer customer = new Customer(name,surName,gender,dob,address,email,id);


    }
}
