package BTEP.person;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Artist extends Employee{

    private  String instrument;

//    public Artist(String instrument) {
//        super();
//        this.instrument = instrument;
//    }

    public Artist(String name, String surname, Gender gender, LocalDate dob, String address,
                  String email, BigDecimal hourlyWage, String instrument) {
        super(name, surname, gender, dob, address, email, hourlyWage);
        this.instrument = instrument;
    }
}
